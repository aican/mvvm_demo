# mvvm_demo

#### Description
mvvm框架的demo。精简删除google官方demo的工厂模式。


数据分为：

data层: 获取api数据并解析成data层model

domain层： 处理成ui层model

ui层： 展示数据

使用dagger、databingding、rxjava、rxandroid等
