package com.example.mvvmdemo.ui.viewmodel;

import com.example.mvvmdemo.data.network.TestSchedulerProvider;
import com.example.mvvmdemo.domain.repository.ApiDataRepository;
import com.example.mvvmdemo.domain.usecase.MainUseCase;
import com.example.mvvmdemo.ui.model.TestModelVm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.junit.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class MainViewModeTest {

    @Mock
    Context context;

    @Mock
    private ApiDataRepository apiDataRepository;

    private MainViewMode mainViewMode;

    @Before
    public void setUp() throws Exception {

        MainUseCase useCase = new MainUseCase(new TestSchedulerProvider(), apiDataRepository);
        mainViewMode = new MainViewMode(useCase);
    }

    @Test
    public void getJumpObservable() {
    }

    @Test
    public void success() {
        List<TestModelVm> testModelVmList=new ArrayList<>();
        TestModelVm modelVm = new TestModelVm("1", "jestrn", "123");
        testModelVmList.add(modelVm);
        Mockito.when(apiDataRepository.testApi()).thenReturn(Observable.just(testModelVmList));

        mainViewMode.success();
        assertNotNull(mainViewMode.successText.get());
    }

    @Test
    public void onBtn1Click() {
    }

    @Test
    public void onBtn2Click() {
    }

    @Test
    public void onBtn3Click() {
    }
}