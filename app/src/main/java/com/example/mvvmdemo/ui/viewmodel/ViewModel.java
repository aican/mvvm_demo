package com.example.mvvmdemo.ui.viewmodel;

import com.example.mvvmdemo.domain.usecase.UseCase;
import com.example.mvvmdemo.ui.model.ToastVm;

import android.databinding.BaseObservable;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ViewModel extends BaseObservable {

    private final UseCase useCase;

    protected final PublishSubject<ToastVm> toastSubject;

    protected ViewModel(UseCase useCase) {
        this.useCase = useCase;
        toastSubject = PublishSubject.create();
    }

    public Observable<ToastVm> getToastSubject() {
        return toastSubject.hide();
    }

    public void destory() {
        if (useCase != null) {
            useCase.unsubscribe();
        }
    }

}
