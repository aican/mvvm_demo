package com.example.mvvmdemo.ui.aty;

import com.example.mvvmdemo.AppApplication;
import com.example.mvvmdemo.R;
import com.example.mvvmdemo.databinding.AtyListBinding;
import com.example.mvvmdemo.ui.adapter.ListAdapter;
import com.example.mvvmdemo.ui.viewmodel.ListViewModel;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

public class ListActivity extends BaseActivity<ListViewModel> {

    private AtyListBinding viewBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewBinding = DataBindingUtil
            .inflate(LayoutInflater.from(this), R.layout.aty_list, null, false);
        setContentView(viewBinding.getRoot());
        inject();
        initView();
    }

    private void initView() {
        ListAdapter listAdapter = new ListAdapter();
        viewBinding.rvList.setAdapter(listAdapter);
        viewBinding.rvList.setLayoutManager(new LinearLayoutManager(this));
        getViewModel().getData();
        viewBinding.setViewModel(getViewModel());
    }

    private void inject() {
        ((AppApplication) getApplication()).getApplicationComponent().inject(this);
    }
}
