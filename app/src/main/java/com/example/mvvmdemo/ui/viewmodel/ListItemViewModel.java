package com.example.mvvmdemo.ui.viewmodel;

import com.example.mvvmdemo.ui.model.ListVm;

import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import javax.inject.Inject;

public class ListItemViewModel extends BaseObservable {

    public final ObservableBoolean checkedField;

    public final ObservableField<String> nameField;

    private ListVm model;

    @Inject
    public ListItemViewModel() {
        checkedField = new ObservableBoolean();
        nameField = new ObservableField<>();
        checkedField.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                model.setSelected(checkedField.get());
            }
        });
    }

    public void setModel(ListVm model){
        this.model = model;
        checkedField.set(model.isSelected());
        nameField.set(model.getName());
    }
}
