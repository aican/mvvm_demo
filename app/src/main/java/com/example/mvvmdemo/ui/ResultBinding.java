package com.example.mvvmdemo.ui;

import com.example.mvvmdemo.ui.adapter.ListAdapter;
import com.example.mvvmdemo.ui.model.ListVm;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class ResultBinding {

    @BindingAdapter({"listItems"})
    public static void setListItems(RecyclerView recyclerView,
        List<ListVm> data) {
        ListAdapter adapter = (ListAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.setData(data);
        }
    }
}
