package com.example.mvvmdemo.ui;

import com.example.mvvmdemo.domain.model.TestModel;
import com.example.mvvmdemo.ui.model.TestModelVm;

public class Mapper {

    public static TestModelVm modelMappper(TestModel testModel) {
        return new TestModelVm(testModel.getId(), testModel.getName(),
            testModel.getCode());
    }

}
