package com.example.mvvmdemo.ui.model;

public class TestModelVm {
   //可以添加任意字段  tag 等等，给ui使用
    private String id;

    private String name;

    private String code;

    public TestModelVm(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
