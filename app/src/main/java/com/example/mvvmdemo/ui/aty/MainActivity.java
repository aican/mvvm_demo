package com.example.mvvmdemo.ui.aty;

import com.example.mvvmdemo.AppApplication;
import com.example.mvvmdemo.R;
import com.example.mvvmdemo.databinding.AtyMainBinding;
import com.example.mvvmdemo.ui.viewmodel.MainViewMode;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;

public class MainActivity extends BaseActivity<MainViewMode> {

    private AtyMainBinding viewBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil
            .inflate(LayoutInflater.from(this), R.layout.aty_main, null, false);
        setContentView(viewBinding.getRoot());
        inject();
        bindBaseViewModel(getViewModel());
        initView();
    }

    @SuppressLint("CheckResult")
    private void initView() {
        viewBinding.setViewModel(getViewModel());
        addSubscriber(getViewModel().getJumpObservable()
            .subscribe(s -> startActivity(new Intent(this, ListActivity.class))));
    }

    private void inject() {
        ((AppApplication) getApplication()).getApplicationComponent().inject(this);
    }

}
