package com.example.mvvmdemo.ui.viewmodel;

import com.example.mvvmdemo.domain.usecase.UseCase;
import com.example.mvvmdemo.ui.model.ListVm;

import android.databinding.ObservableField;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ListViewModel extends ViewModel {

    public final ObservableField<List<ListVm>> listObservableField;

    @Inject
    protected ListViewModel(UseCase widgetDataSource) {
        super(widgetDataSource);
        listObservableField = new ObservableField<>();
    }

    public void getData() {
        List<ListVm> list = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            list.add(new ListVm(i + "", false));
        }
        listObservableField.set(list);
    }

}
