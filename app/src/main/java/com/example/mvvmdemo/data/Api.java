package com.example.mvvmdemo.data;

import com.example.mvvmdemo.domain.model.ResponseBaseModel;
import com.example.mvvmdemo.domain.model.TestModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface Api {

    @GET("/api/ads_position/lists")
    Observable<ResponseBaseModel<List<TestModel>>> testApi();

    @GET("/api/sign/GetTodayRanklist")
    Observable<ResponseBaseModel<Object>> testErrorApi();
}
