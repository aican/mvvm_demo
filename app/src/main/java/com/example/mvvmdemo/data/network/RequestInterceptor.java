package com.example.mvvmdemo.data.network;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor{
    private final Context context;

    public RequestInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder();
        //TODO 添加headers
        builder.addHeader("token", "");
        Request request = builder.method(original.method(), original.body()).build();
        return chain.proceed(request);
    }
}
