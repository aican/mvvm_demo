package com.example.mvvmdemo.domain.usecase;

import com.example.mvvmdemo.data.network.SchedulerProvider;
import com.example.mvvmdemo.domain.repository.ApiDataRepository;
import com.example.mvvmdemo.domain.repository.ApiRepository;
import com.example.mvvmdemo.ui.model.TestModelVm;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MainUseCase extends UseCase implements ApiRepository {

    private final ApiDataRepository apiDataRepository;

    @Inject
    public MainUseCase(SchedulerProvider schedulerProvider, ApiDataRepository apiDataRepository) {
        super(schedulerProvider);
        this.apiDataRepository = apiDataRepository;
    }

    @Override
    public Observable<List<TestModelVm>> testApi() {
        return apiDataRepository.testApi();
    }

    @Override
    public Observable<List> testErrorApi() {
        return apiDataRepository.testErrorApi();
    }


}
