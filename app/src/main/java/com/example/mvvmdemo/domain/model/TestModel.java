package com.example.mvvmdemo.domain.model;

import com.google.gson.annotations.SerializedName;

public class TestModel {
    //要做到与api 字段一一对应  不能添加与减少字段

    private String id;

    private String name;

    @SerializedName("code")
    private String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
