package com.example.mvvmdemo.domain.repository;

import com.example.mvvmdemo.ui.model.TestModelVm;

import java.util.List;

import io.reactivex.Observable;

public interface ApiRepository {

    Observable<List<TestModelVm>> testApi();

    Observable<List> testErrorApi();

}
