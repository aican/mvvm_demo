package com.example.mvvmdemo;

import com.example.mvvmdemo.dagger.ApplicationComponent;

import android.support.multidex.MultiDexApplication;

public class AppApplication extends MultiDexApplication {

    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
    }

    private void initApplicationComponent() {
        applicationComponent = ApplicationComponent.Initializer.init(this);
    }

}
