package com.example.mvvmdemo.dagger;

import com.google.gson.Gson;

import com.example.mvvmdemo.AppApplication;
import com.example.mvvmdemo.ui.adapter.ListAdapter;
import com.example.mvvmdemo.ui.aty.ListActivity;
import com.example.mvvmdemo.ui.aty.MainActivity;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

@SuppressWarnings("ALL")
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(MainActivity activity);

    void inject(ListActivity activity);

    Context context();

    Gson gson();

    void inject(ListAdapter.ViewHolder holder);


    final class Initializer {

        private Initializer() {
        }

        public static ApplicationComponent init(AppApplication appApplication) {
            return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(appApplication))
                .build();
        }
    }
}
