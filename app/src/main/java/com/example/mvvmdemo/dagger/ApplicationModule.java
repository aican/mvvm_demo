package com.example.mvvmdemo.dagger;

import com.google.gson.Gson;

import com.example.mvvmdemo.data.network.AppSchedulerProvider;
import com.example.mvvmdemo.data.network.HttpDataRepository;
import com.example.mvvmdemo.data.network.SchedulerProvider;
import com.example.mvvmdemo.domain.repository.ApiDataRepository;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    Gson gson() {
        return new Gson();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider(
        AppSchedulerProvider appSchedulerProvider) {
        return appSchedulerProvider;
    }

    @Provides
    @Singleton
    ApiDataRepository provideHttpDataRepository(
        HttpDataRepository httpDataRepository) {
        return httpDataRepository;
    }

}
